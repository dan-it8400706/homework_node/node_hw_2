"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var card_class_1 = require("./card.class");
var currency_enum_1 = require("./currency.enum");
var transaction_class_1 = require("./transaction.class");
var card = new card_class_1.Card();
// card.AddTransaction(100, CurrencyEnum.USD);
// card.AddTransaction(200, CurrencyEnum.UAH);
card.AddTransaction(new transaction_class_1.Transaction(100, currency_enum_1.CurrencyEnum.USD));
card.AddTransaction(new transaction_class_1.Transaction(200, currency_enum_1.CurrencyEnum.UAH));
var transaction = card.GetTransaction('12345'); // undefined
var balanceUAH = card.GetBalance(currency_enum_1.CurrencyEnum.UAH); // 200
console.log(transaction);
console.log(balanceUAH);
