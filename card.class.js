"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Card = void 0;
var transaction_class_1 = require("./transaction.class");
var Card = /** @class */ (function () {
    function Card() {
        this.transactions = [];
    }
    // public AddTransaction(amount: number, currency: CurrencyEnum): string;
    Card.prototype.AddTransaction = function (arg) {
        if (typeof arg === 'object' && 'id' in arg) {
            this.transactions.push(arg);
            return arg.getId();
        }
        else {
            var transaction = new transaction_class_1.Transaction(arg.amount, arg.currency);
            return this.AddTransaction(transaction);
        }
    };
    Card.prototype.GetTransaction = function (id) {
        // return this.transactions.find((transaction) => transaction.getId() === id);
        for (var _i = 0, _a = this.transactions; _i < _a.length; _i++) {
            var transaction = _a[_i];
            if (transaction.getId() === id) {
                return transaction;
            }
        }
        return undefined;
    };
    Card.prototype.GetBalance = function (currency) {
        return this.transactions
            .filter(function (transaction) { return transaction.currency === currency; })
            .reduce(function (sum, transaction) { return sum + transaction.amount; }, 0);
    };
    return Card;
}());
exports.Card = Card;
