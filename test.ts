import { Card } from './card.class';
import { CurrencyEnum } from './currency.enum';
import { Transaction } from './transaction.class'

const card = new Card();

// card.AddTransaction(100, CurrencyEnum.USD);
// card.AddTransaction(200, CurrencyEnum.UAH);

card.AddTransaction(new Transaction(100, CurrencyEnum.USD));
card.AddTransaction(new Transaction(200, CurrencyEnum.UAH));

const transaction = card.GetTransaction('12345'); // undefined
const balanceUAH = card.GetBalance(CurrencyEnum.UAH); // 200

console.log(transaction);
console.log(balanceUAH);