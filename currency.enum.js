"use strict";
// export enum CurrencyEnum {
//     USD = 'USD',
//     UAH = 'UAH',
//   }
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrencyEnum = void 0;
var CurrencyEnum;
(function (CurrencyEnum) {
    CurrencyEnum["USD"] = "USD";
    CurrencyEnum["UAH"] = "UAH";
})(CurrencyEnum || (exports.CurrencyEnum = CurrencyEnum = {}));
