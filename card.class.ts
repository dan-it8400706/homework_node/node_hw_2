import { Transaction } from './transaction.class';
import { CurrencyEnum } from './currency.enum';

export class Card {
  private readonly transactions: Array<Transaction>;

  constructor() {
    this.transactions = [];
  }


public AddTransaction(transaction: Transaction): string;
// public AddTransaction(amount: number, currency: CurrencyEnum): string;
public AddTransaction(
    arg: Transaction | { amount: number; currency: CurrencyEnum }
  ): string {
    if (typeof arg === 'object' && 'id' in arg) {
      this.transactions.push(arg);
      return arg.getId();
    } else {
      const transaction = new Transaction(arg.amount, arg.currency);
      return this.AddTransaction(transaction);
    }
  }

  public GetTransaction(id: string): Transaction | undefined {
    // return this.transactions.find((transaction) => transaction.getId() === id);
    for (const transaction of this.transactions) {
      if (transaction.getId() === id) {
        return transaction;
      }
    }
    
    return undefined;
  }

  public GetBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter((transaction) => transaction.currency === currency)
      .reduce((sum, transaction) => sum + transaction.amount, 0);
  }
}
